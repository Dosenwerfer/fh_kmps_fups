object KMPSP2 {

  import scala.io.Source

  case class Track(title: String, length: String, rating: Int, features: List[String], writers: List[String])
  case class Album(title: String, date: String, artist: String, tracks: List[Track])

  def main(args: Array[String]): Unit = {
    val input = Source.fromFile("alben.xml").toList
    val tokens = createTokenList(input, Nil, "")
    println(tokens)
    val albums = parseFile(tokens, Nil)
    println(albums)
    println("prod(3, 5): " + prod(x => x, 3, 5))
    println("prodCurry(x => x, 3, 5): " + prodCurry(x => x)(3, 5))
    println("prodInt(3, 5): " + prodInt(3, 5))
    println("faculty(4): " + faculty(4))
  }

  def append[T](xs: List[T], y: T): List[T] = xs match {
    case Nil => y :: xs
    case x :: xs => x :: append(xs, y)
  }

  // Aufgabe 2
  def createTokenList(xs: List[Any], token: List[String], word: String): List[String] = xs match {
    case Nil => token
    case ('<' | '>') :: xsr => word match {
      case "" => createTokenList(xsr, token, word)
      case _ => createTokenList(xsr, token :+ word, "")
    }
    case ('\n' | '\r' | '\t') :: xsr => createTokenList(xsr, token, word)
    case y :: ys => createTokenList(ys, token, word + y)
  }

  // Aufgabe 3
  def parseFile(tokenList: List[String], result: List[Album]): List[Album] = tokenList match {
    case Nil => result
    case "album" :: restToken => val alb = parseAlbum(restToken, Album("", "", "", Nil))
      parseFile(alb._1, result :+ alb._2)
    case _ :: restToken => parseFile(restToken, result)
  }

  def parseAlbum(tokenList: List[String], alb: Album): (List[String], Album) = tokenList match {
    case "/album" :: zs => (zs, alb)
    case "title" :: x :: restToken => parseAlbum(restToken, alb.copy(title = x))
    case "date" :: x :: restToken => parseAlbum(restToken, alb.copy(date = x))
    case "artist" :: x :: restToken => parseAlbum(restToken, alb.copy(artist = x))
    case "track" :: restToken => parseAlbum(parseTrack(restToken, Track("", "", 0, Nil, Nil))._1,
      alb.copy(tracks = append(alb.tracks, parseTrack(restToken, Track("", "", 0, Nil, Nil))._2)))
    case _ :: restToken => parseAlbum(restToken, alb)
  }

  def parseTrack(tokenList: List[String], track: Track): (List[String], Track) = tokenList match {
    case "/track" :: xs => (xs, track)
    case "title" :: x :: restToken => parseTrack(restToken, track.copy(title = x))
    case "length" :: x :: restToken => parseTrack(restToken, track.copy(length = x))
    case "rating" :: x :: restToken => parseTrack(restToken, track.copy(rating = x.toInt))
    case "writing" :: x :: restToken => parseTrack(restToken, track.copy(writers = append(track.writers, x)))
    case "feature" :: x :: restToken => parseTrack(restToken, track.copy(features = append(track.writers, x)))
    case _ :: restToken => parseTrack(restToken, track)
  }

  // 4a
  def prod(f: Int => Int, a: Int, b: Int): Int = if (a > b) 1 else f(a) * prod(f, a + 1, b)

  // 4b
  def prodCurry(f: Int => Int): (Int, Int) => Int = prod(f, _, _)

  // 4c
  def prodInt = prodCurry(x => x)

  // 4d
  def faculty(x: Int): Int = prodInt(1, x)
}
