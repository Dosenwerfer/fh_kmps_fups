import scala.io.Source

/**
 * Created by Jacques Schneider on 20-Nov-19.
 */
object KMPSP3 {

  case class Track(title: String, length: String, rating: Int, features: List[String], writers: List[String])
  case class Album(title: String, date: String, artist: String, tracks: List[Track])

  def main(args: Array[String]): Unit = {
    val input = Source.fromFile("alben.xml").toList
    val tokens = createTokenList2(input)
    println("Token list:")
    println(tokens)
    val albums = parseFile(tokens, Nil)
    println("Album titles to upper case:")
    println(albumTitlesToUpperCase(albums))
    println("Album and track titles to upper case:")
    println(albumTrackTitlesToUpperCase(albums))
    println("Track lengths:")
    println(trackLengths(albums))

    // Michael Jackson album
    println("Thriller:")
    val thriller = albums(1)
    println(thriller)
    println("4-star tracks from Thriller:")
    println(fourStarRating(thriller))
    println("Tracks of Rod Temperton:")
    println(tracksOfTemperton(thriller))

    println("Partition test:")
    println(partition[Int](List(1,2,3,2,8,1,2,8,8,3,4,8,4), x => x == 8))
    println("Split at Thriller:")
    val thrillerSplit = splitAtThriller(thriller)
    println(thrillerSplit._1)
    println(thrillerSplit._2)

    println(myfold[Int](x => x, 1, 1, 5, (m1, m2) => m1 * m2))

//    println("prod(3, 5): " + prod(x => x, 3, 5))
//    println("prodCurry(x => x, 3, 5): " + prodCurry(x => x)(3, 5))
//    println("prodInt(3, 5): " + prodInt(3, 5))
//    println("faculty(4): " + faculty(4))
  }

  def append[T](xs: List[T], y: T): List[T] = xs match {
    case Nil => y :: xs
    case x :: xs => x :: append(xs, y)
  }

  // Aufgabe 2
  def createTokenList(xs: List[Char], token: List[String], word: String): List[String] = xs match {
    case Nil => token
    case ('<' | '>') :: xsr => word match {
      case "" => createTokenList(xsr, token, word)
      case _ => createTokenList(xsr, token :+ word, "")
    }
    case ('\n' | '\r' | '\t') :: xsr => createTokenList(xsr, token, word)
    case y :: ys => createTokenList(ys, token, word + y)
  }

  // Aufgabe 3
  def parseFile(tokenList: List[String], result: List[Album]): List[Album] = tokenList match {
    case Nil => result
    case "album" :: restToken => val alb = parseAlbum(restToken, Album("", "", "", Nil))
      parseFile(alb._1, result :+ alb._2)
    case _ :: restToken => parseFile(restToken, result)
  }

  def parseAlbum(tokenList: List[String], alb: Album): (List[String], Album) = tokenList match {
    case "/album" :: zs => (zs, alb)
    case "title" :: x :: restToken => parseAlbum(restToken, alb.copy(title = x))
    case "date" :: x :: restToken => parseAlbum(restToken, alb.copy(date = x))
    case "artist" :: x :: restToken => parseAlbum(restToken, alb.copy(artist = x))
    case "track" :: restToken => parseAlbum(parseTrack(restToken, Track("", "", 0, Nil, Nil))._1,
      alb.copy(tracks = append(alb.tracks, parseTrack(restToken, Track("", "", 0, Nil, Nil))._2)))
    case _ :: restToken => parseAlbum(restToken, alb)
  }

  def parseTrack(tokenList: List[String], track: Track): (List[String], Track) = tokenList match {
    case "/track" :: xs => (xs, track)
    case "title" :: x :: restToken => parseTrack(restToken, track.copy(title = x))
    case "length" :: x :: restToken => parseTrack(restToken, track.copy(length = x))
    case "rating" :: x :: restToken => parseTrack(restToken, track.copy(rating = x.toInt))
    case "writing" :: x :: restToken => parseTrack(restToken, track.copy(writers = append(track.writers, x)))
    case "feature" :: x :: restToken => parseTrack(restToken, track.copy(features = append(track.writers, x)))
    case _ :: restToken => parseTrack(restToken, track)
  }

  // 4a
  def prod(f: Int => Int, a: Int, b: Int): Int = if (a > b) 1 else f(a) * prod(f, a + 1, b)

  // 4b
  def prodCurry(f: Int => Int): (Int, Int) => Int = prod(f, _, _)

  // 4c
  def prodInt = prodCurry(x => x)

  // 4d
  def faculty(x: Int): Int = prodInt(1, x)


  // P3

  // Aufgabe 1a
  def map[T](input_list: List[T], func:(T) => T): List[T] = input_list match {
    case Nil => Nil
    case x :: xs => func(x) :: map[T](xs, func)
  }

  // Aufgabe 1b
  def albumTitlesToUpperCase(albums: List[Album]): List[Album] = {
    map[Album](albums, album => album.copy(title = album.title.toUpperCase()))
  }

  // Aufgabe 1c
  def albumTrackTitlesToUpperCase(albums: List[Album]): List[Album] = {
    map[Album](albums, album => album.copy(
      title = album.title.toUpperCase(),
      tracks = map[Track](album.tracks, track => track.copy(
        title = track.title.toUpperCase()))))
  }

  // Aufgabe 1d
  def poly_map[T,U](input_list: List[T], func:(T) => U): List[U] = input_list match {
    case Nil => Nil
    case x :: xs => func(x) :: poly_map(xs, func)
  }

  // Aufgabe 1e
  def trackLengths(albums: List[Album]): List[List[String]] = {
    poly_map[Album, List[String]](albums,
      album => poly_map[Track,String](album.tracks,
        track => track.length))
  }

  // Aufgabe 2a
  def filter[T](input_list: List[T], condition:(T) => Boolean): List[T] = input_list match {
    case Nil => Nil
    case x :: xs => if (condition(x)) x :: filter[T](xs, condition) else filter[T](xs, condition)
  }

  // Aufgabe 2b
  def fourStarRating(album: Album): List[Track] = {
    filter[Track](album.tracks, track => track.rating >= 4)
  }

  // Aufgabe 2c
  def tracksOfTemperton(album: Album): List[String] = {
    poly_map[Track,String](
      filter[Track](album.tracks,
        track => filter[String](track.writers, writer => writer == "Rod Temperton").nonEmpty),
      track => track.title)
  }

  // Aufgabe 3a
  def partition[T](input_list: List[T], condition:(T) => Boolean): List[List[T]] = {
    def partitionSingle(input_list: List[T], temp: List[T]): List[List[T]] = input_list match {
      case Nil => temp :: Nil
      case x :: xs =>
        if (condition(x))
          temp :: partitionSingle(xs, Nil)
        else
          partitionSingle(xs, temp :+ x)
    }
    partitionSingle(input_list, Nil)
  }

  // Aufgabe 3b
  def splitAtThriller(album: Album): (List[Track], List[Track]) = {
    val result = partition[Track](album.tracks, track => track.title == "Thriller")
    (result(0), result(1))
  }

  // Aufgabe 3c
  def isBlank(s: String): Boolean = s.trim.isEmpty
  def createTokenList2(input_list: List[Char]): List[String] = {
    val partitioned = partition[Char](input_list, c => c == '<' || c == '>' || c == '\n' || c == '\r' || c == '\t')
    val polymapped = poly_map[List[Char], String](partitioned, cs => cs.mkString)
    val filtered = filter[String](polymapped, s => !isBlank(s))
    filtered
  }

  // Aufgabe 4a
  def myfold[T](f: Int => T, start: T, a: Int, b: Int, merge: (T, T) => T): T = {
    if (a > b)
      start
    else
      merge(f(a), myfold(f, start, a + 1, b, merge))
  }

  // Aufgabe 4b: left folding. Verhalten wäre unterschiedlich wenn merge nicht kommutativ ist

  // Aufgabe 4c: Return start value or IllegalArgumentException

  // Aufgabe 4d
  def mapUebung(f: Int => Int, xs: List[Int]): List[Int] = xs match {
    case Nil => Nil
    case y :: ys => f(y) :: mapUebung(f, ys)
  }
  def foldUebung(f:(Int, Int) => Int, start: Int, xs: List[Int]) : Int = xs match {
    case x::Nil => f(start, x) // Bei leerer Liste Rückgabe von start
    case h::ts => foldUebung(f, f(start, h), ts)
  }
  def range(a: Int, b: Int) : List[Int] = if (a > b) Nil else a::range(a + 1,b)

  def myfoldShort(f: Int => Int, start: Int, a: Int, b: Int, merge: (Int, Int) => Int): Int = {
    foldUebung(merge, start, mapUebung(x => f(x), range(a, b)))
  }

  // Aufgabe 4e
}
