import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;

/**
 * Created by js6673s on 20-Oct-19.
 */
public class Funktional {

    public static void main( String... args ) throws IOException, URISyntaxException {
        byte[] file_contents = Files.readAllBytes( Path.of(
                Funktional.class.getClassLoader().getResource( "alben.xml" ).toURI() ) );
        ArrayList<String> tokens = createTokenList( file_contents );
        System.out.println( tokens );
        ArrayList<Album> albums = parseFile( tokens );
        System.out.println( albums );
    }

    public static ArrayList<String> createTokenList( byte[] input ) {
        return createTokenList( input, new ArrayList<>(), 0 );
    }

    public static ArrayList<Album> parseFile( ArrayList<String> tokens ) {
        return parseFile( tokens, new ArrayList<>(), 0 );
    }

    private static ArrayList<String> createTokenList( byte[] input, ArrayList<String> tokens, int current_character ) {
        if ( current_character < input.length ) {
            if ( input[current_character] == '\n' || input[current_character] == '\r' || input[current_character] == '\t' ) {
                return createTokenList( input, tokens, current_character + 1 );
            } else if ( new String( input, current_character, 7, StandardCharsets.UTF_8 ).equals( "<album>" ) ) {
                tokens.add( "album" );
                return createTokenList( input, tokens, current_character + 7 );
            } else if ( new String( input, current_character, 8, StandardCharsets.UTF_8 ).equals( "</album>" ) ) {
                tokens.add( "/album" );
                return createTokenList( input, tokens, current_character + 8 );
            } else if ( new String( input, current_character, 7, StandardCharsets.UTF_8 ).equals( "<title>" ) ) {
                tokens.add( "title" );
                current_character += 7;
                int title_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, title_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/title" );
                return createTokenList( input, tokens, current_character + title_length + 8 );
            } else if ( new String( input, current_character, 8, StandardCharsets.UTF_8 ).equals( "<artist>" ) ) {
                tokens.add( "artist" );
                current_character += 8;
                int artist_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, artist_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/artist" );
                return createTokenList( input, tokens, current_character + artist_length + 9 );
            } else if ( new String( input, current_character, 8, StandardCharsets.UTF_8 ).equals( "<rating>" ) ) {
                tokens.add( "rating" );
                current_character += 8;
                int rating_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, rating_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/rating" );
                return createTokenList( input, tokens, current_character + rating_length + 9 );
            } else if ( new String( input, current_character, 7, StandardCharsets.UTF_8 ).equals( "<track>" ) ) {
                tokens.add( "track" );
                return createTokenList( input, tokens, current_character + 7 );
            } else if ( new String( input, current_character, 8, StandardCharsets.UTF_8 ).equals( "</track>" ) ) {
                tokens.add( "/track" );
                return createTokenList( input, tokens, current_character + 9 );
            } else if ( new String( input, current_character, 9, StandardCharsets.UTF_8 ).equals( "<feature>" ) ) {
                tokens.add( "feature" );
                current_character += 9;
                int feature_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, feature_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/feature" );
                return createTokenList( input, tokens, current_character + feature_length + 10 );
            } else if ( new String( input, current_character, 8, StandardCharsets.UTF_8 ).equals( "<length>" ) ) {
                tokens.add( "length" );
                current_character += 8;
                int length_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, length_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/length" );
                return createTokenList( input, tokens, current_character + length_length + 9 );
            } else if ( new String( input, current_character, 9, StandardCharsets.UTF_8 ).equals( "<writing>" ) ) {
                tokens.add( "writing" );
                current_character += 9;
                int writing_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, writing_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/writing" );
                return createTokenList( input, tokens, current_character + writing_length + 10 );
            } else if ( new String( input, current_character, 6, StandardCharsets.UTF_8 ).equals( "<date>" ) ) {
                tokens.add( "date" );
                current_character += 6;
                int date_length = length( input, current_character, 0 );
                tokens.add( new String( input, current_character, date_length, StandardCharsets.UTF_8 ) );
                tokens.add( "/date" );
                return createTokenList( input, tokens, current_character + date_length + 7 );
            }
        }
        return tokens;
    }

    private static int length( byte[] input, int start, int length ) {
        if ( input[start + length] == '<' ) {
            return length;
        } else {
            return length( input, start, length + 1 );
        }
    }

    private static ArrayList<Album> parseFile( ArrayList<String> tokens, ArrayList<Album> albums, int index ) {
        if ( index < tokens.size() ) {
            if ( tokens.get( index ).equals( "album" ) ) {
                albums.add( createAlbum( tokens, index + 1, new Album() ) );
            }
            parseFile( tokens, albums, index + 1 );
        }
        return albums;
    }

    private static Album createAlbum( ArrayList<String> tokens, int index, Album album ) {
        if ( !tokens.get( index ).equals( "/album" ) ) {
            switch ( tokens.get( index ) ) {
                case "track":
                    index++;
                    Object[] result = createTrack( tokens, index, new Track() );
                    album.tracks.add( (Track) result[1] );
                    index = (int) result[0] - 1;
                    break;
                case "artist":
                    index++;
                    album.artist = tokens.get( index );
                    break;
                case "title":
                    index++;
                    album.title = tokens.get( index );
                    break;
                case "date":
                    index++;
                    album.date = tokens.get( index );
                    break;
            }
            return createAlbum( tokens, index + 2, album );
        } else {
            return album;
        }
    }

    private static Object[] createTrack( ArrayList<String> tokens, int index, Track track ) {
        if ( !tokens.get( index ).equals( "/track" ) ) {
            switch ( tokens.get( index ) ) {
                case "writing":
                    index++;
                    track.writers.add( tokens.get( index ) );
                    break;
                case "feature":
                    index++;
                    track.features.add( tokens.get( index ) );
                    break;
                case "title":
                    index++;
                    track.title = tokens.get( index );
                    break;
                case "length":
                    index++;
                    track.length = tokens.get( index );
                    break;
                case "rating":
                    index++;
                    track.rating = Integer.parseInt( tokens.get( index ) );
                    break;
            }
            index += 2;
            return createTrack( tokens, index, track );
        } else {
            return new Object[]{index, track};
        }
    }
}
